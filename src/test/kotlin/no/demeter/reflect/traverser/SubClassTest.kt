package no.demeter.reflect.traverser

import no.demeter.reflect.traverser.testutils.RecordingVisitor
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class SubClassTest {

    @Test
    internal fun subclass() {
        val bean = TestBean()

        val result = mutableListOf<String>()
        BeanTraverser.traverse(bean, RecordingVisitor(result))

        val expected = listOf(
                "-> .",
                "-> .bean",
                "<- .bean",
                "-> .bloop",
                "<- .bloop",
                "-> .middle",
                "<- .middle",
                "-> .top",
                "<- .top",
                "<- ."
        )

        assertThat(result).containsExactlyElementsOf(expected)
    }

    abstract class Top {
        val top = ""
        abstract val bloop: Int
    }

    abstract class Middle : Top() {
        val middle = ""
    }

    class TestBean : Middle() {
        val bean = ""
        override val bloop: Int = 5
    }
}