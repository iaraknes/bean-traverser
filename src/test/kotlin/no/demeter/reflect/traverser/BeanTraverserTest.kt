package no.demeter.reflect.traverser

import org.junit.jupiter.api.Test
import java.lang.IllegalStateException

class BeanTraverserTest {
    @Test
    internal fun xx() {

        val bean = TestBean("", 1, subBeans = listOf(
                SubBean("X"),
                SubBean("Y"),
                SubBean("Z")
        ))

        BeanTraverser.traverse(bean, object : BeanVisitor {
            override fun visit(ctx: BeanVisitorContext) {
                println("--> ${ctx.path}")
                val value = ctx.value
                if(value is SubBean) {
                    value.name = "MODIFIED"
                }
            }

            override fun leave(ctx: BeanVisitorContext) {
                println("<-- ${ctx.path}")
            }
        },
                filters = listOf(ExceptionFilter())
        )
        println(bean)
    }

    data class TestBean(val string: String, val int: Int, val subBeans: List<SubBean>)

    data class SubBean(var name: String) {
        val nuts: String
            get() = throw IllegalStateException("You called getNuts()!")
    }
}

class ExceptionFilter : BeanVisitorFilter {
    override fun shouldVisit(ctx: BeanVisitorContext): Boolean {
        try {
            ctx.value
            return true
        } catch (e: Exception) {
            println("SKIPPING ${ctx.path}")
            return false
        }
    }
}