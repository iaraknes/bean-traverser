package no.demeter.reflect.traverser.testutils

import no.demeter.reflect.traverser.BeanVisitor
import no.demeter.reflect.traverser.BeanVisitorContext

class DebugVisitor(private val visitor: BeanVisitor) : BeanVisitor {
    override fun visit(ctx: BeanVisitorContext) {
        println("---> ${ctx.path}")
        visitor.visit(ctx)
    }

    override fun leave(ctx: BeanVisitorContext) {
        println("<--- ${ctx.path}")
        super.leave(ctx)
    }
}