package no.demeter.reflect.traverser.testutils

import no.demeter.reflect.traverser.BeanVisitor
import no.demeter.reflect.traverser.BeanVisitorContext

class RecordingVisitor(val output: MutableList<String> = mutableListOf(), prefix: String? = null) : BeanVisitor {

    private val finalPrefix = if(prefix == null) "" else "$prefix "

    override fun visit(ctx: BeanVisitorContext) {
        output.add("$finalPrefix-> ${ctx.path}")
    }

    override fun leave(ctx: BeanVisitorContext) {
        output.add("$finalPrefix<- ${ctx.path}")
    }
}