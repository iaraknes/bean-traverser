package no.demeter.reflect.traverser

import no.demeter.reflect.traverser.testutils.RecordingVisitor
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class VisitAndLeaveOrderingTest {

    @Test
    internal fun ordering() {

        val bean = TestBean(
                string = "something",
                int = 9876,
                list = listOf(
                        SubBean(name = "name", list = listOf()),
                        SubBean(name = "name", list = listOf())
                ),
                array = arrayOf(
                        SubBean(name = "name", list = listOf()),
                        SubBean(name = "name", list = listOf(1, 2, 3))
                )
        )

        val result = mutableListOf<String>()

        BeanTraverser.traverse(bean, RecordingVisitor(result))

        val expected = listOf(
                "-> .",
                "-> .array",
                "-> .array[0]",
                "-> .array[0].list",
                "<- .array[0].list",
                "-> .array[0].name",
                "<- .array[0].name",
                "<- .array[0]",
                "-> .array[1]",
                "-> .array[1].list",
                "-> .array[1].list[0]",
                "<- .array[1].list[0]",
                "-> .array[1].list[1]",
                "<- .array[1].list[1]",
                "-> .array[1].list[2]",
                "<- .array[1].list[2]",
                "<- .array[1].list",
                "-> .array[1].name",
                "<- .array[1].name",
                "<- .array[1]",
                "<- .array",
                "-> .int",
                "<- .int",
                "-> .list",
                "-> .list[0]",
                "-> .list[0].list",
                "<- .list[0].list",
                "-> .list[0].name",
                "<- .list[0].name",
                "<- .list[0]",
                "-> .list[1]",
                "-> .list[1].list",
                "<- .list[1].list",
                "-> .list[1].name",
                "<- .list[1].name",
                "<- .list[1]",
                "<- .list",
                "-> .string",
                "<- .string",
                "<- ."
        )

        assertThat(result).containsExactlyElementsOf(expected)
    }

    data class TestBean(
            val string: String,
            val int: Int,
            val list: List<SubBean>,
            val array: Array<SubBean>
    )

    data class SubBean(var name: String, val list: List<Int>)
}