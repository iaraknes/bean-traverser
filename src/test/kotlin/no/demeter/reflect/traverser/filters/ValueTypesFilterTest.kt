package no.demeter.reflect.traverser.filters

import no.demeter.reflect.traverser.BeanTraverser
import no.demeter.reflect.traverser.BeanVisitor
import no.demeter.reflect.traverser.BeanVisitorContext
import org.junit.jupiter.api.Test
import java.time.LocalDate

internal class ValueTypesFilterTest {
    @Test
    internal fun name() {
        val bean = ValueBean(LocalDate.EPOCH)
        BeanTraverser.traverse(
                bean,
                filters = listOf(ValueTypesFilter(LocalDate::class.java)),
                visitor = object : BeanVisitor {
                    override fun visit(ctx: BeanVisitorContext) {
                        println("${ctx.path}")
                    }
                })
    }
}

data class ValueBean(val date: LocalDate)