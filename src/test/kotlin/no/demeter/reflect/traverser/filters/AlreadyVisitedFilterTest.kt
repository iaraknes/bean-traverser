package no.demeter.reflect.traverser.filters

import no.demeter.reflect.traverser.BeanTraverser
import no.demeter.reflect.traverser.BeanVisitor
import no.demeter.reflect.traverser.BeanVisitorContext
import org.junit.jupiter.api.Test

internal class AlreadyVisitedFilterTest {
    @Test
    internal fun name() {
        val b = B()
        val a = A(b = b)
        b.a = a

        BeanTraverser.traverse(
                bean = a,
                visitor = visitor,
                filters = listOf(AlreadyVisitedFilter(false))
        )
    }

    object visitor : BeanVisitor {
        override fun visit(ctx: BeanVisitorContext) {
            println(ctx.path)
        }
    }
}

class A(val str: String = "x", var b: B)
class B(val str: String = "y", var a: A? = null)