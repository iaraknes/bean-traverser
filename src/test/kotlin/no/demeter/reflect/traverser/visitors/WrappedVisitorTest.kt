package no.demeter.reflect.traverser.visitors

import no.demeter.reflect.traverser.BeanTraverser
import no.demeter.reflect.traverser.BeanVisitor
import no.demeter.reflect.traverser.testutils.RecordingVisitor
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class WrappedVisitorTest {
    @Test
    internal fun name() {
        val bean = TestBean("x", 1)

        val result = mutableListOf<String>()

        val wrappedVisitor = object : WrappedVisitor() {
            override val visitor: BeanVisitor = RecordingVisitor(result)
        }

        BeanTraverser.traverse(bean, wrappedVisitor)
        assertThat(result).containsExactly(
                "-> .",
                "-> .name",
                "<- .name",
                "-> .value",
                "<- .value",
                "<- ."
        )
    }

    @Test
    internal fun example() {
        val bean = TestBean("x", 42)

        val result = mutableListOf<String>()
        val visitor = ExampleVisitor(result)
        BeanTraverser.traverse(bean, visitor)

        assertThat(result).containsExactly(
                "-> .",
                "Entering TestBean",
                "-> .name",
                "= x",
                "<- .name",
                "-> .value",
                "= 42",
                "<- .value",
                "Leaving TestBean",
                "<- ."
        )
    }

    data class TestBean(val name: String, val value: Int)

    class ExampleVisitor(val output: MutableList<String>) : WrappedVisitor() {

        override val visitor: BeanVisitor =
                ComposedVisitor(
                        RecordingVisitor(output),
                        ConfigurableBeanVisitor.Builder()
                                .onVisit(this::visitInt)
                                .onVisit(this::visitString)
                                .onVisit(this::visitTestBean)
                                .onLeave(this::leaveTestBean)
                                .build()
                )

        private fun visitTestBean(bean: TestBean) {
            output.add("Entering TestBean")
        }

        private fun leaveTestBean(bean: TestBean) {
            output.add("Leaving TestBean")
        }

        private fun visitString(string: String) {
            output.add("= $string")
        }

        private fun visitInt(int: Int) {
            output.add("= $int")
        }
    }
}