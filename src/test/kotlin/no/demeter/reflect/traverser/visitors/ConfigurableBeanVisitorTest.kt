package no.demeter.reflect.traverser.visitors

import no.demeter.reflect.traverser.BeanTraverser
import org.junit.jupiter.api.Test

internal class ConfigurableBeanVisitorTest {
    @Test
    internal fun xx() {
        val interceptor = ExampleInterceptor()
        val bean = TestBean(a = TestBeanA("wow", null), b = TestBeanB(42, null))

        interceptor.intercept(bean)

        println(bean.a)
        println(bean.b)
    }
}

class ExampleInterceptor {
    private val visitor = ConfigurableBeanVisitor.Builder()
            .onVisit(this::visitA)
            .onVisit(this::visitB)
            .build()

    fun intercept(bean: Any) {
        BeanTraverser.traverse(bean, visitor)
    }

    private fun visitA(bean: TestBeanA) {
        bean.link = TestLink("linkA/${bean.name}")
    }

    private fun visitB(bean: TestBeanB) {
        bean.link = TestLink("linkB/${bean.num}")
    }
}

data class TestBean(val a: TestBeanA, val b: TestBeanB)

data class TestBeanA(val name: String, var link: TestLink?)
data class TestBeanB(val num: Int, var link: TestLink?)

data class TestLink(val href: String)