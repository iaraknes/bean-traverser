package no.demeter.reflect.traverser.visitors

import no.demeter.reflect.traverser.BeanTraverser
import no.demeter.reflect.traverser.testutils.RecordingVisitor
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class ComposedVisitorTest {
    @Test
    internal fun name() {
        val bean = TestBean(1, 2)
        val result = mutableListOf<String>()

        val visitor = ComposedVisitor(
                RecordingVisitor(result, prefix = "A"),
                RecordingVisitor(result, prefix = "B")
        )

        BeanTraverser.traverse(bean, visitor)

        assertThat(result).containsExactly(
                "A -> .",
                "B -> .",

                "A -> .a",
                "B -> .a",
                "B <- .a",
                "A <- .a",

                "A -> .b",
                "B -> .b",
                "B <- .b",
                "A <- .b",

                "B <- .",
                "A <- ."
        )
    }

    data class TestBean(val a: Int, val b: Int)
}