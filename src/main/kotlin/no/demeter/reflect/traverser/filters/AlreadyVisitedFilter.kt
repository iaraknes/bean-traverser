package no.demeter.reflect.traverser.filters

import no.demeter.reflect.traverser.BeanVisitorContext
import no.demeter.reflect.traverser.BeanVisitorFilter

class AlreadyVisitedFilter(private val shouldVisit: Boolean = true) : BeanVisitorFilter {
    private val visited: MutableSet<Any> = HashSet()

    override fun shouldVisit(ctx: BeanVisitorContext): Boolean {
        val value = ctx.value
                ?: return true

        if(visited.contains(value)) {
            ctx.abortRecursion()
            return shouldVisit
        } else {
            visited.add(value)
            return true
        }
    }
}