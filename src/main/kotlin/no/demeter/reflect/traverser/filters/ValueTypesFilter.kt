package no.demeter.reflect.traverser.filters

import no.demeter.reflect.traverser.BeanVisitorContext
import no.demeter.reflect.traverser.BeanVisitorFilter
import java.lang.Exception

class ValueTypesFilter(vararg val valueTypes: Class<*>) : BeanVisitorFilter {
    override fun shouldVisit(ctx: BeanVisitorContext): Boolean {
        val value = try {
            ctx.value
        } catch (e: Exception) {
            return true
        }

        if(value == null) {
            return true
        }

        for(clazz in valueTypes) {
            if(clazz.isAssignableFrom(value.javaClass)) {
                ctx.abortRecursion()
                return true
            }
        }

        return true
    }
}