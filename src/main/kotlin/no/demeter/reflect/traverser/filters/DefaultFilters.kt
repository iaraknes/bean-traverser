package no.demeter.reflect.traverser.filters

import no.demeter.reflect.traverser.BeanVisitorFilter
import java.time.temporal.Temporal

object DefaultFilters {
    val javaTime: BeanVisitorFilter = ValueTypesFilter(Temporal::class.java)

    val ALL = listOf(javaTime)
}