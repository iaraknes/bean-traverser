package no.demeter.reflect.traverser

import no.demeter.reflect.traverser.filters.AlreadyVisitedFilter
import no.demeter.reflect.traverser.internal.PropertyContext
import java.lang.Exception
import java.lang.reflect.Method
import java.util.*

object BeanTraverser {
    fun traverse(bean: Any?, onVisit: (BeanVisitorContext) -> Unit) {
        traverse(bean, object : BeanVisitor {
            override fun visit(ctx: BeanVisitorContext) {
                onVisit(ctx)
            }
        })
    }
    fun traverse(bean: Any?, visitor: BeanVisitor, filters: List<BeanVisitorFilter> = listOf(), maxDepth: Int = 1000) {
        BeanTraverserIterator(bean, visitor, filters, maxDepth).run()
    }
}

private class BeanTraverserIterator(
        private val bean: Any?,
        private val visitor: BeanVisitor,
        private val filters: List<BeanVisitorFilter>,
        private val maxDepth: Int?
) {
    private var depth = 0
    private val stack: Stack<TraverseOperation> = Stack()

    fun run() {
        val ctx = PropertyContext(
                parent = null,
                getter = { bean }
        )

        deferVisit(ctx)

        loop()
    }

    private fun loop() {
        var op: TraverseOperation? = nextOperation()
        while (op != null) {
            when (op) {
                is TraverseOperation.Visit -> {
                    visit(op.ctx)
                }
                is TraverseOperation.Iterate -> {
                    if(op.hasNext()) {
                        val ctx = op.next()
                        if(op.hasNext()) {
                            stack.push(op)
                        }
                        visit(ctx)
                    }
                }
                is TraverseOperation.Leave -> {
                    visitor.leave(op.ctx)
                    depth--
                }
            }

            op = nextOperation()
        }
    }

    private fun deferVisit(ctx: PropertyContext) {
        stack.push(TraverseOperation.Visit(ctx))
    }

    private fun deferLeave(ctx: PropertyContext) {
        stack.push(TraverseOperation.Leave(ctx))
    }

    private fun nextOperation(): TraverseOperation? {
        if(stack.empty()) {
            return null
        }
        return stack.pop()
    }

    private fun visit(ctx: PropertyContext) {
        for(filter in filters) {
            if(!filter.shouldVisit(ctx)) {
                return
            }
        }

        depth++
        if(maxDepth != null && depth > maxDepth) {
            throw BeanTraverserException(ctx, "Max depth of $maxDepth exceeded")
        }

        deferLeave(ctx)

        try {
            visitor.visit(ctx)

            if(!ctx.recursionAborted) {
                visitChildren(ctx)
            }
        } catch (e: BeanTraverserException) {
            throw e
        } catch (e: Exception) {
            throw BeanTraverserException(ctx, e.message, e)
        }
    }

    private fun visitChildren(parent: PropertyContext) {
        val obj = parent.value
                ?: return

        if(isSimpleProperty(obj)) {
            return
        }

        when(obj) {
            is Array<*> -> {
                visitArray(parent, obj)
            }

            is Collection<*> -> {
                visitCollection(parent, obj)
            }

            // todo: map

            else -> {
                visitBean(parent, obj)
            }
        }
    }

    private fun visitBean(parent: PropertyContext, bean: Any) {
        val methods = bean.javaClass.methods
        methods.sortByDescending { it.name }

        for(method in methods) {
            val propertyName = propertyName(method)
                    ?: continue

            if(propertyName == "class") {
                continue
            }

            val setterName = setterName(propertyName)
            val setter: Method?  = try {
                bean.javaClass.getMethod(setterName, method.returnType)
            } catch (e: NoSuchMethodException) {
                null
            }

            val ctx = PropertyContext(
                    parent = parent,
                    propertyName = propertyName,
                    type = method.returnType,
                    getter = { method.invoke(bean) },
                    setter = { setter!!.invoke(it) }
            )
            deferVisit(ctx)
        }
    }

    private fun visitArray(parent: PropertyContext, array: Array<*>) {
        stack.push(TraverseOperation.Iterate(parent, array.iterator()))
    }

    private fun visitCollection(parent: PropertyContext, collection: Collection<*>) {
        stack.push(TraverseOperation.Iterate(parent, collection.iterator()))
    }

    private fun isSimpleProperty(obj: Any?): Boolean {
        return obj is String
                || obj is Boolean
                || obj is Int
                || obj is Long
                || obj is Float
                || obj is Double
    }

    private fun propertyName(method: Method): String? {
        if(method.parameters.isNotEmpty()) {
            return null
        }
        if(method.returnType == Void.TYPE) {
            return null
        }

        val name = method.name

        // todo: regex
        if(name.startsWith("get")) {
            val withoutPrefix = name.removePrefix("get")
            return withoutPrefix.replaceRange(0, 1, withoutPrefix.firstOrNull()?.toLowerCase()?.toString() ?: "")
        }
        if(name.startsWith("is") && method.returnType == true.javaClass) {
            val withoutPrefix = name.removePrefix("is")
            return withoutPrefix.replaceRange(0, 1, withoutPrefix.firstOrNull()?.toLowerCase()?.toString() ?: "")
        }

        return null
    }

    private fun setterName(propertyName: String): String {
        return "set" + propertyName.replaceRange(0, 1, propertyName.firstOrNull()?.toUpperCase()?.toString() ?: "")
    }
}

private sealed class TraverseOperation {
    data class Visit(val ctx: PropertyContext) : TraverseOperation()
    data class Leave(val ctx: PropertyContext) : TraverseOperation()

    class Iterate(private val parent: PropertyContext, private val iterator: Iterator<*>) : TraverseOperation(), Iterator<PropertyContext> {
        private var collectionIndex = 0

        override fun hasNext(): Boolean {
            return iterator.hasNext()
        }

        override fun next(): PropertyContext {
            val obj = iterator.next()

            val ctx = PropertyContext(
                    parent = parent,
                    collectionIndex = collectionIndex,
                    getter = { obj })

            collectionIndex++

            return ctx
        }
    }
}
