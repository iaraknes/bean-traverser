package no.demeter.reflect.traverser

interface BeanVisitorContext {
    var value: Any?
    val propertyName: String?
    val type: Class<*>?

    val collectionIndex: Int?
    // todo: mapKey

    val path: String
    val parent: BeanVisitorContext?

    fun abortRecursion()
}