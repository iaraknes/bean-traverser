package no.demeter.reflect.traverser

interface BeanVisitor {
    fun visit(ctx: BeanVisitorContext) { }
    fun leave(ctx: BeanVisitorContext) { }
}