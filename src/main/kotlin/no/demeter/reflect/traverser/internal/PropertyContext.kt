package no.demeter.reflect.traverser.internal

import no.demeter.reflect.traverser.BeanTraverserException
import no.demeter.reflect.traverser.BeanVisitorContext
import java.lang.reflect.InvocationTargetException

internal class PropertyContext(
        override val parent: PropertyContext?,
        override val propertyName: String? = null,
        override val type: Class<*>? = null,
        override val collectionIndex: Int? = null,

        private val getter: () -> Any?,
        private val setter: ((Any?) -> Unit)? = null
) : BeanVisitorContext {

    var recursionAborted = false

    override var value: Any?
        get() {
            try {
                return getter()
            } catch (e: InvocationTargetException) {
                val cause = e.cause
                throw BeanTraverserException(this, "Getter threw exception: $cause", cause)
            }
        }

        set(value) {
            val setter = this.setter
            if(setter != null) {
                setter(value)
            } else {
                throw BeanTraverserException(this, "No setter exists for property")
            }
        }

    // todo: hasSetter: boolean

    override fun abortRecursion() {
        recursionAborted = true
    }

    override val path: String = calculatePath()

    private fun calculatePath(): String {
        val parents = ArrayList<BeanVisitorContext>()
        var p = parent
        while(p != null) {
            parents.add(p)
            p = p.parent
        }
        parents.reverse()

        val sb = StringBuilder()
        for(ctx in parents) {
            if(ctx.collectionIndex != null) {
                sb.append('[')
                sb.append(ctx.collectionIndex!!)
                sb.append(']')
            } else {
                if(ctx.propertyName != null) {
                    sb.append('.')
                    sb.append(ctx.propertyName)
                }
            }
        }

        if(collectionIndex != null) {
            sb.append('[')
            sb.append(collectionIndex!!)
            sb.append(']')
        } else {
            sb.append('.')
            if(propertyName != null) {
                sb.append(propertyName)
            }
        }
        return sb.toString()
    }
}