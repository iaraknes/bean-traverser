package no.demeter.reflect.traverser

class BeanTraverserException(ctx: BeanVisitorContext, reason: String?, cause: Throwable? = null)
    : RuntimeException("${ctx.path}: $reason", cause)