package no.demeter.reflect.traverser

interface BeanVisitorFilter {
    fun shouldVisit(ctx: BeanVisitorContext): Boolean
}