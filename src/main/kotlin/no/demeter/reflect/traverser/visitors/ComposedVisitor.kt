package no.demeter.reflect.traverser.visitors

import no.demeter.reflect.traverser.BeanVisitor
import no.demeter.reflect.traverser.BeanVisitorContext

class ComposedVisitor(private vararg val visitors: BeanVisitor) : BeanVisitor {
    override fun visit(ctx: BeanVisitorContext) {
        visitors.forEach { it.visit(ctx) }
    }

    override fun leave(ctx: BeanVisitorContext) {
        for (i in visitors.indices.reversed()) {
            visitors[i].leave(ctx)
        }
    }
}