package no.demeter.reflect.traverser.visitors

import no.demeter.reflect.traverser.BeanVisitor
import no.demeter.reflect.traverser.BeanVisitorContext

abstract class WrappedVisitor : BeanVisitor {
    protected abstract val visitor: BeanVisitor

    override fun visit(ctx: BeanVisitorContext) {
        visitor.visit(ctx)
    }

    override fun leave(ctx: BeanVisitorContext) {
        visitor.leave(ctx)
    }
}