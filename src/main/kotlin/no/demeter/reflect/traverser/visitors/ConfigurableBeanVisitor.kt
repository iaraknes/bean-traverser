package no.demeter.reflect.traverser.visitors

import no.demeter.reflect.traverser.BeanVisitor
import no.demeter.reflect.traverser.BeanVisitorContext

class ConfigurableBeanVisitor private constructor(
        private val typeVisitors: List<Pair<Class<*>, (Any) -> Unit>>,
        private val typeLeavers: List<Pair<Class<*>, (Any) -> Unit>>
) : BeanVisitor {
    override fun visit(ctx: BeanVisitorContext) {
        val value = ctx.value
                ?: return
        val type = value.javaClass

        for((clazz, handler) in typeVisitors) {
            if(clazz.isAssignableFrom(type)) {
                handler(value)
            }
        }
    }

    override fun leave(ctx: BeanVisitorContext) {
        val value = ctx.value
                ?: return
        val type = value.javaClass

        for((clazz, handler) in typeLeavers) {
            if(clazz.isAssignableFrom(type)) {
                handler(value)
            }
        }
    }

    class Builder {
        private val typeVisitors: MutableList<Pair<Class<*>, (Any) -> Unit>> = mutableListOf()
        private val typeLeavers: MutableList<Pair<Class<*>, (Any) -> Unit>> = mutableListOf()

        fun <T>onVisit(clazz: Class<T>, handler: (T) -> Unit): Builder {
            typeVisitors.add(Pair(clazz, handler as (Any) -> Unit))
            return this
        }

        inline fun <reified T> onVisit(noinline handler: (T) -> Unit): Builder {
            onVisit(T::class.java, handler)
            return this
        }

        fun <T>onLeave(clazz: Class<T>, handler: (T) -> Unit): Builder {
            typeLeavers.add(Pair(clazz, handler as (Any) -> Unit))
            return this
        }

        inline fun <reified T> onLeave(noinline handler: (T) -> Unit): Builder {
            onLeave(T::class.java, handler)
            return this
        }

        fun build(): ConfigurableBeanVisitor {
            return ConfigurableBeanVisitor(
                    typeVisitors = ArrayList(typeVisitors),
                    typeLeavers = ArrayList(typeLeavers))
        }
    }
}